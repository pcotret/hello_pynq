# Hello Pynq!
This tutorial has been written on behalf of two 3rd year Supélec students:

* Aliénor Lebel, [alienor.lebel@supelec.fr](mailto:alienor.lebel@supelec.fr)
* Marion Le Penven, [marion.lepenven@supelec.fr](mailto:marion.lepenven@supelec.fr)

This work has been made in the context of a short-term project under the supervision of Pascal Cotret, associate professor at CentraleSupélec Rennes ([pascal.cotret@centralesupelec.fr](mailto:pascal.cotret@centralesupelec.fr))

# Outline
1. [Goals of this tutorial](https://bitbucket.org/pcotret/hello_pynq/overview#markdown-header-goals-of-this-tutorial)
2. [Prerequisites](https://bitbucket.org/pcotret/hello_pynq/overview#markdown-header-prerequisites)
3. [Step-by-step script construction](https://bitbucket.org/pcotret/hello_pynq/overview#markdown-header-step-by-step-script-construction)    
    3.1. [Default project](https://bitbucket.org/pcotret/hello_pynq/overview#markdown-header-default-project)    
    3.2. [DDR memory issue](https://bitbucket.org/pcotret/hello_pynq/overview#markdown-header-ddr-memory-issue)    
    3.3. [UART settings](https://bitbucket.org/pcotret/hello_pynq/overview#markdown-header-uart-settings)    
    3.4. [What about our clock settings?](https://bitbucket.org/pcotret/hello_pynq/overview#markdown-header-what-about-our-clock-settings)
4. [TCL script optimization](https://bitbucket.org/pcotret/hello_pynq/overview#markdown-header-tcl-script-optimization)
5. [Deliverables](https://bitbucket.org/pcotret/hello_pynq/overview#markdown-header-deliverables)
6. [References](https://bitbucket.org/pcotret/hello_pynq/overview#markdown-header-references)

## Goals of this tutorial
In this tutorial, we'll see how to build the most simple Vivado project for the PYNQ board (www.pynq.io). The goal is to run the simple "hello world" function on one of the ARM Cortex-A9 cores available on the PYNQ, nothing more!

**Short version: if you want a plug-and-play Vivado project for the PYNQ board, just run the Makefile. If you want to know how it was built, read the following ;)**

On the official Github repository, there is already a Makefile to generate a full Vivado project containing lots of IPs in the PL (3 Microblaze cores, audio/video...) (see **[3]**). Unfortunately, this project has a few drawbacks:

* The Makefile runs on Linux. We're still on Windows, sorry :p
* It has been made for Vivado 2016.1. We are working on 2016.4...
* The original project was **really** long to be generated on a basic laptop.

Furthermore, we wanted to make a simple TCL script (without version dependencies) to generate everything automatically instead of a dummy copy-paste of the project.

In the following, we'll see how we built this script from scratch as we were unboxing the PYNQ for the first time.

## Prerequisites
* You have a PYNQ board :)
* 2016.1 project compiled from the Makefile available at https://github.com/Xilinx/PYNQ/tree/master/Pynq-Z1/vivado/base. Afterwards, imported in a 2016.4 Vivado install

## Step-by-step script construction
In this tutorial, it is supposed that our codes are stored in the folder **D:/hello_pynq**. Here is a tree view of this folder:

* readme.md: this README
* github_project: the original project from PYNQ
* my_project: our project

### Default project
First of all, we generated a simple 2016.4 Vivado project for the PYNQ device (xc7z020clg400-1). The block design is only composed of a default ZYNQ7 Processing System with an UART IP enabled:

![block_design.png](https://bitbucket.org/repo/yx9ayb/images/2284708253-block_design.png)
![uart_config.png](https://bitbucket.org/repo/yx9ayb/images/734837847-uart_config.png)

Let's generate the bitstream and create an "hello world" project in Xilinx SDK. When we want to run the program, we get the following error:

![sdk_memory_error.png](https://bitbucket.org/repo/yx9ayb/images/1053106877-sdk_memory_error.png)

### DDR memory issue
Strange, isn't it ? In fact, if we look at the memory mapping, the faulty address is the address space of the DDR external memory:

![memory_map.png](https://bitbucket.org/repo/yx9ayb/images/600465214-memory_map.png)

Is our DDR chip burnt ? No! Let's have a look at DDR configuration section in **my_project\my_project.sdk\design_1_wrapper_hw_platform_0\ps7_init.html** (left side: our project; right side: Xilinx reference project). 

![ddr_config.png](https://bitbucket.org/repo/yx9ayb/images/3760499503-ddr_config.png)

If we compare to the original project, we can see that the DDR in PYNQ is a custom part! Now, we just have to customize it in our block design. Double-click on the Processing System and go to **DDR configuration**

![ddr_parameterrs.png](https://bitbucket.org/repo/yx9ayb/images/3409260265-ddr_parameterrs.png)

Now, we just have to modify parameters that can be found in the HTML mentioned above. And re-run the bitstream generation...

### UART settings
Then, if we run again our "hello world" project in the SDK, nothing appears in the terminal even if the COM port is correctly connected.
By looking again at the reference project, we can see that the UART is configured as follows (just double-click on the Processing System in the block design and then go in **Peripheral I/O Pins** to get into this configuration window).

![uart_config.png](https://bitbucket.org/repo/yx9ayb/images/2535851382-uart_config.png)

Once again, let's go for a new bitstream generation and SDK run. The result in the terminal was not the expected one...

![hello_fake.png](https://bitbucket.org/repo/yx9ayb/images/3973581925-hello_fake.png)

We have some characters but they are basicaally random stuff. Usually, this problem is due to a wrong baudrate. We check the UART settings: 115200 bps, 1 stop bit => Nope, nothing wrong there :/

### What about our clock settings?
If we look back at our Processing System in the block diagram, there is a *small* difference between the reference project and the homemade one. In our case, the reference clock frequency was 33MHz, not 50!

![clock_settings.png](https://bitbucket.org/repo/yx9ayb/images/1364630733-clock_settings.png)

Finally, after a last bitstream generation, our project behaves correctly and displays an "hello world!" message executed on an ARM core of the PYNQ !

![hello_world.png](https://bitbucket.org/repo/yx9ayb/images/2544515960-hello_world.png)

We have one last thing to do: everything ran on a given laptop, what if we want to run it somewhere else? Copy/paste the full project? We rather decided to create a TCL script that is independent from the laptop.

## TCL script optimization
Vivado has a TCL script generation command (**File=>Write Project Tcl...**). We just had to clean it and be sure that everything was fine.

## Deliverables
* *hello_pynq.tcl* script that generates the default project.
* *src/bd/design_1.tcl*: generating the block design.
## References

1. **Official website:** www.pynq.io
2. **Github repo:** https://github.com/Xilinx/PYNQ
3. **Reference project (Vivado 2016.1):** https://github.com/Xilinx/PYNQ/tree/master/Pynq-Z1/vivado/base
4. **Vivado user guide:** https://www.xilinx.com/support/documentation/sw_manuals/xilinx2016_4/ug973-vivado-release-notes-install-license.pdf